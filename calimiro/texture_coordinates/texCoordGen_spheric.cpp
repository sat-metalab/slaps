/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/texture_coordinates/texCoordGen_spheric.h"

namespace calimiro
{

std::vector<glm::vec2> TexCoordGenSpheric::computeTextureCoordinates()
{
    _log->message("Generating spheric texture coordinates with the following parameters:"
                  "\n  Eye position (%, %, %)"
                  "\n  Eye direction (%, %, %)"
                  "\n  Horizon rotation: %deg"
                  "\n  Flip horizontal: %"
                  "\n  Flip vertical: %",
        std::to_string(_eye_position.x),
        std::to_string(_eye_position.y),
        std::to_string(_eye_position.z),
        std::to_string(_eye_direction.x),
        std::to_string(_eye_direction.y),
        std::to_string(_eye_direction.z),
        std::to_string(_horizon_rotation),
        (_flip_horizontal ? "TRUE" : "FALSE"),
        (_flip_vertical ? "TRUE" : "FALSE"));

    std::vector<glm::vec2> new_uvs;

    const auto transformation_matrix_3d = getUVtransformationMatrix();
    const auto transformation_matrix_2d =
        glm::mat3(1.f, 0.f, -0.5f, 0.f, 1.f, -0.5f, 0.f, 0.f, 1.f) * getFlipMatrix() * getHorizonBiasMatrix() * glm::mat3(1.f, 0.f, 0.5f, 0.f, 1.f, 0.5f, 0.f, 0.f, 1.f);

    for (const auto& vertex : _vertices)
    {
        const auto point = -glm::vec3(glm::vec4(vertex, 1.f) * transformation_matrix_3d);
        const auto unit = glm::normalize(point);

        const auto u = 0.5f + std::atan2(unit.x, unit.z) / (2.f * M_PIf32);
        const auto v = 0.5f + std::asin(unit.y) * M_1_PIf32;

        const auto uv_homogeneous = glm::vec3(u, v, 1.f) * transformation_matrix_2d;
        const auto uv = glm::vec2(uv_homogeneous.x, uv_homogeneous.y);
        new_uvs.push_back(uv);
    }

    return new_uvs;
};

} // namespace calimiro
