/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/texture_coordinates/texCoordGen_equirectangular.h"

namespace calimiro
{

std::vector<glm::vec2> TexCoordGenEquirectangular::computeTextureCoordinates()
{
    _log->message("Generating equirectangular texture coordinates with the following parameters:"
                  "\n  Eye position (%, %, %)"
                  "\n  Eye direction (%, %, %)"
                  "\n  Horizon rotation: %deg"
                  "\n  Flip horizontal: %"
                  "\n  Flip vertical: %"
                  "\n  Field of view: %deg",
        std::to_string(_eye_position.x),
        std::to_string(_eye_position.y),
        std::to_string(_eye_position.z),
        std::to_string(_eye_direction.x),
        std::to_string(_eye_direction.y),
        std::to_string(_eye_direction.z),
        std::to_string(_horizon_rotation),
        (_flip_horizontal ? "TRUE" : "FALSE"),
        (_flip_vertical ? "TRUE" : "FALSE"),
        std::to_string(_fov));

    std::vector<glm::vec2> new_uvs;

    const auto transformation_matrix_3d = getUVtransformationMatrix();
    const auto transformation_matrix_2d =
        glm::mat3(1.f, 0.f, -0.5f, 0.f, 1.f, -0.5f, 0.f, 0.f, 1.f) * getFlipMatrix() * getHorizonBiasMatrix() * glm::mat3(1.f, 0.f, 0.5f, 0.f, 1.f, 0.5f, 0.f, 0.f, 1.f);

    // Rotate the breaking point (think International Date Line) to the edge of the shape
    const auto fov_rad = (-_fov * 0.5f) * constants::deg_to_rad;
    const auto origin_for_longitude = glm::vec2(-std::sin(fov_rad), std::cos(fov_rad));

    for (const auto& vertex : _vertices)
    {
        const auto point = glm::vec4(vertex, 1.f) * transformation_matrix_3d;
        const auto unit = glm::normalize(glm::vec3(point));

        const auto vector_for_longitude = glm::vec2(point.x, point.z);
        const auto length = glm::length(vector_for_longitude);
        const auto unit_for_longitude = length < FLT_EPSILON ? glm::vec2(0.f, 0.f) : glm::normalize(vector_for_longitude);
        const auto offset = length < FLT_EPSILON ? M_PI_2f32 : 0.f;

        const auto angle_for_longitude = glm::dot(origin_for_longitude, unit_for_longitude);
        const auto crossp = glm::cross(glm::vec3(origin_for_longitude, 0.f), glm::vec3(unit_for_longitude, 0.f));
        const auto vector_equals = glm::all(glm::equal(crossp, glm::vec3(0.f, 0.f, 0.f)));
        const auto longitude = (crossp.z > 0.f || vector_equals ? std::acos(angle_for_longitude) : 2.f * M_PIf32 - std::acos(angle_for_longitude) - offset) * 0.5f * M_1_PIf32;
        const auto latitude = std::fma(unit.y , 0.5f, 0.5f);

        const auto uv_homogeneous = glm::vec3(longitude, latitude, 1.f) * transformation_matrix_2d;
        const auto uv = glm::vec2(uv_homogeneous.x, uv_homogeneous.y);
        new_uvs.push_back(uv);
    }

    return new_uvs;
};

} // namespace calimiro
