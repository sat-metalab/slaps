/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_PATTERN__
#define __CALIMIRO_PATTERN__

#include <cstdint>
#include <filesystem>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/structured_light.hpp>

#include "./calimiro/abstract_logger.h"
#include "./calimiro/workspace.h"

namespace calimiro
{
class Pattern
{
  public:
    // Constructor
    Pattern(AbstractLogger* log, double scale = 1.0)
        : _log(log)
        , _scale(scale){};

    /**
     * Generates a gray code pattern in function of the projector's resolution
     * \param width Projector width
     * \param height Projector height
     * \return Return a vector of all frames constituting the structured light pattern
     */
    virtual std::vector<cv::Mat> create(uint32_t width, uint32_t height) = 0;

    /**
     *  Decodes a sequence of structured light pattern from a vector of images
     * \param width Projector width
     * \param height Projector height
     * \param images Pattern captures
     * \return Return an array containing pixel coordinates from source structured light pattern
     */
    virtual std::optional<cv::Mat2i> decode(uint32_t width, uint32_t height, std::vector<cv::Mat1b> images) = 0;

  protected:
    AbstractLogger* _log;
    double _scale;
};

class Structured_Light : public Pattern
{
  public:
    Structured_Light(AbstractLogger* log, double scale = 1.0)
        : Pattern(log, scale){};

    std::vector<cv::Mat> create(uint32_t width, uint32_t height) final;
    std::optional<cv::Mat2i> decode(uint32_t width, uint32_t height, std::vector<cv::Mat1b> images) final;

    /**
     * Return the last computed shadow mask, if any
     * \return Return the shadow mask
     */
    std::optional<cv::Mat1b> getShadowMask() const;

    /**
     * Return the pairs of images for x and y coordinates
     * \param width Width of the original pattern
     * \param height Height of the original pattern
     * \return Return two images containing x and y coordinates
     */
    std::optional<std::pair<cv::Mat1b, cv::Mat1b>> getDecodedCoordinates(int width, int height) const;

  private:
    std::optional<cv::Mat1b> _shadowmask;
    std::optional<cv::Mat2i> _decoded_image;

    cv::Mat1b computeShadowMask(const cv::Mat1b& black_image, const cv::Mat1b& white_image, size_t threshold);
    cv::Mat2i decodeImage(const cv::Ptr<cv::structured_light::GrayCodePattern>& graycode, const std::vector<cv::Mat1b>& pattern, const cv::Mat1b& shadow_mask);
};
}; // namespace calimiro

#endif // __CALIMIRO_PATTERN__
