/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/mapXYZ.h"

#include <glm/glm.hpp>
#include <jsoncpp/json/json.h>
#include <openMVG/sfm/sfm_data.hpp>
#include <openMVG/sfm/sfm_data_io.hpp>
#include <opencv2/opencv.hpp>

#include "./calimiro/calibration_point.h"
#include "./calimiro/constants.h"

namespace calimiro
{

MapXYZs::MapXYZs(AbstractLogger* log, std::filesystem::path p)
{
    _log = log;
    _workspace = p;
    std::ifstream fs(_workspace / constants::cOutputDirectory / "matches.json");
    if (!fs.is_open())
    {
        _log->error("MapXYZs::MapXYZs() - % could not be opened", _workspace / constants::cOutputDirectory / "matches.json");
        return;
    }

    Json::CharReaderBuilder jsonReader;
    std::string errs;
    Json::Value obj;
    if (!Json::parseFromStream(jsonReader, fs, &obj, &errs))
    {
        _log->error("MapXYZs::MapXYZs() - Unable to parse file %", _workspace / constants::cOutputDirectory / "matches.json");
        _log->error(errs.c_str());
        return;
    }

    const Json::Value& structures = obj["structure"]; // array of landmarks
    _matches.reserve(structures.size());

    // read the json file and store all the matches
    for (unsigned int i = 0; i < structures.size(); ++i)
    {
        _matches.push_back({structures[i]["value"]["observations"][0]["key"].asInt(),
            structures[i]["value"]["observations"][0]["value"]["x"][0].asInt(),
            structures[i]["value"]["observations"][0]["value"]["x"][1].asInt(),
            structures[i]["value"]["X"][0].asDouble(),
            structures[i]["value"]["X"][1].asDouble(),
            structures[i]["value"]["X"][2].asDouble(),
            -1, // we don't know the projector's point correspondance and ID yet
            -1,
            -1});
    }
    if (_matches.empty())
    {
        _log->warning("MapXYZs::MapXYZs() - matches.json contains no matches");
    }
}

void MapXYZs::pixelToProj(double scale)
{
    // load the sfm data scene to get the key to filename correspondence
    openMVG::sfm::SfM_Data sfm_data;
    if (!openMVG::sfm::Load(sfm_data, _workspace / constants::cOutputDirectory / constants::cSfM_data_filename, openMVG::sfm::ESfM_Data(openMVG::sfm::VIEWS)))
    {
        _log->error("MapXYZs::pixelToProj: The input file \"%\" cannot be read", _workspace / constants::cOutputDirectory / constants::cSfM_data_filename);
    }

    // group the matches by position number
    std::sort(_matches.begin(), _matches.end(), [](const match& lhs, const match& rhs) { return lhs.position < rhs.position; });

    std::map<int, std::vector<match>> matches_by_pos;
    std::for_each(_matches.begin(), _matches.end(), utils::inserter<int, match>(matches_by_pos, [](match a) { return a.position; }));

    cv::Mat3i decodedMat; // decodedMat stores the correspondance between the pixel point and the projector point
    cv::FileStorage fs;
    _matches.clear(); // clears the vector, will be filled back with the projector's corresponding pt
    for (const auto& pos : matches_by_pos)
    {
        _log->message("------%-------", std::to_string(pos.first));
        const std::filesystem::path sView_filename = sfm_data.views.at(pos.first).get()->s_Img_path;
        fs.open((_workspace / "decoded_matrix" / sView_filename).string(), cv::FileStorage::READ);
        fs[sView_filename.stem().string()] >> decodedMat;

        for (auto& someMatch : pos.second)
        {
            if (static_cast<int>(decodedMat[someMatch.pixel_y][someMatch.pixel_x][2] != 0))
            {
                _matches.push_back({someMatch.position,
                    someMatch.pixel_x,
                    someMatch.pixel_y,
                    someMatch.x,
                    someMatch.y,
                    someMatch.z,
                    static_cast<int>(decodedMat[someMatch.pixel_y][someMatch.pixel_x][0] / scale + floor(2.0f / scale)),
                    static_cast<int>(decodedMat[someMatch.pixel_y][someMatch.pixel_x][1] / scale + floor(2.0f / scale)),
                    static_cast<int>(decodedMat[someMatch.pixel_y][someMatch.pixel_x][2])});
            }
        }
        fs.release();
    }
}

std::map<int, std::vector<CalibrationPoint>> MapXYZs::sampling(unsigned int nb_pts)
{
    int random_nb;
    srand(time(nullptr)); // initialize random seed for the pseudo random number generator

    // sort and groupby the matches by projector and select n sampling points
    std::sort(_matches.begin(), _matches.end(), [](const match& lhs, const match& rhs) { return lhs.prj_nb < rhs.prj_nb; });

    std::map<int, std::vector<match>> matches_by_proj;
    std::for_each(_matches.begin(), _matches.end(), utils::inserter<int, match>(matches_by_proj, [](match a) { return a.prj_nb; }));

    std::map<int, std::vector<CalibrationPoint>> samples_by_proj;
    std::vector<CalibrationPoint> samples;

    for (const auto& proj : matches_by_proj)
    {
        samples.clear();
        for (unsigned int i = 0; i < nb_pts; ++i)
        {
            random_nb = rand() % proj.second.size();
            samples.emplace_back(
                glm::dvec3{proj.second[random_nb].x, proj.second[random_nb].y, proj.second[random_nb].z}, glm::dvec2{proj.second[random_nb].prj_x, proj.second[random_nb].prj_y});
        }
        samples_by_proj.insert(std::make_pair(proj.first, samples));
    }

    return samples_by_proj;
}

} // namespace calimiro
