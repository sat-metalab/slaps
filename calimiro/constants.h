/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __CALIMIRO_CONSTANTS__
#define __CALIMIRO_CONSTANTS__

namespace calimiro
{
namespace constants
{

constexpr auto cOutputDirectory = "reconstruction_output";
constexpr auto cImage_describer = "image_describer.json";
constexpr auto cSfM_data_filename = "sfm_data.json";
constexpr auto cMatchesDirectory = "matches_output";
constexpr auto cFeatureDirectory = "features_output";
constexpr auto cMatchesPutativeOutput_txt = "matches.putative.txt";
constexpr auto cMatchesPutativeOutput_bin = "matches.putative.bin";
constexpr auto cGraphPutativeMatches = "putative_matches";
constexpr auto cPutativeAdjacencyMatrix_svg = "PutativeAdjacencyMatrix.svg";
constexpr auto cGraphGeometricMatches = "geometric_matches";
constexpr auto cGeometricAdjacencyMatrix_svg = "GeometricAdjacencyMatrix.svg";
constexpr auto cDatabase_path = "/share/calimiro/sensor_width_camera_database.txt";
constexpr auto cMatches_f_bin = "matches.f.bin";
constexpr auto cSfMReconstructionReport_html = "SfMReconstruction_Report.html";
constexpr auto cSfM_data_bin = "sfm_data.bin";
constexpr auto cCloudAndPoses_ply = "cloud_and_poses.ply";
constexpr auto cPointCloudStructureFromKnownPoses_ply = "ptcloud_structure_from_known_poses.ply";
constexpr auto cIntrinsics = "intrinsics.json";
constexpr auto cSfMStructureFromKnownPosesReport_html = "SfMStructureFromKnownPoses_Report.html";
constexpr auto cMatches_json = "matches.json";
constexpr float rad_to_deg = 180.f / M_PIf32;
constexpr float deg_to_rad = M_PIf32 / 180.f;

};     // namespace constants
};     // namespace calimiro
#endif // __CALIMIRO_CONSTANTS__
