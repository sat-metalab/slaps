#include "./calimiro/workspace.h"

#include <cassert>

namespace calimiro
{

bool Workspace::saveImagesFromList(const ImageList& images)
{
    const std::filesystem::path workpath(_work_path);
    for (const auto& image : images)
    {
        auto fullpath = workpath / image.first;
        std::filesystem::create_directories(fullpath.parent_path());
        if (!cv::imwrite(fullpath.string(), image.second))
            return false;
    }
    return true;
}

std::vector<std::string> Workspace::generateStructuredLightCapturePaths(size_t position_count, size_t projector_count, size_t pattern_size) const
{
    assert(position_count > 0 && projector_count > 0 && pattern_size > 0);

    const int padding = 4;
    const std::string extension = ".jpg";

    std::vector<std::string> paths;

    std::vector<std::string> filenames;
    std::string number;
    std::string padded_number;

    for (size_t projector_index = 1; projector_index <= projector_count; ++projector_index)
    {
        for (size_t pattern_index = 1; pattern_index <= pattern_size; ++pattern_index)
        {
            number = std::to_string(pattern_index);
            padded_number = std::string(padding - number.size(), '0') + number;
            filenames.emplace_back("prj" + std::to_string(projector_index) + "_pattern" + padded_number + extension);
        }
    }

    for (size_t position_index = 0; position_index < position_count; ++position_index)
    {
        for (const auto& name : filenames)
        {
            auto position_name = "pos_" + std::to_string(position_index);
            paths.emplace_back(std::filesystem::path(position_name) / name);
        }
    }

    return paths;
}

std::optional<cv::Mat3i> Workspace::combineDecodedProjectors(const std::vector<cv::Mat2i>& decoded_projectors)
{
    if (!decoded_projectors.size())
        return {};

    const auto size = decoded_projectors[0].size();
    for (const auto& decoded : decoded_projectors)
        if (decoded.size() != size)
            return {};

    cv::Mat3i merged = cv::Mat3i::zeros(size);
    int projector_index = 1; // Projector's index must start at 1 not 0
    for (const auto& decoded : decoded_projectors)
    {
        for (int y = 0; y < size.height; ++y)
        {
            for (int x = 0; x < size.width; ++x)
            {
                auto pixel = decoded(y, x);
                auto merged_pixel = merged(y, x);
                if (pixel[0] != 0 && pixel[1] != 0 && merged_pixel[0] == 0 && merged_pixel[1] == 0)
                {
                    merged(y, x)[0] = pixel[0];
                    merged(y, x)[1] = pixel[1];
                    merged(y, x)[2] = projector_index;
                }
            }
        }
        ++projector_index;
    }

    return merged;
}

} // namespace calimiro
