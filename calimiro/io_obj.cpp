/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/io_obj.h"

#include <fstream>
#include <regex>

#include <glm/glm.hpp>

namespace calimiro
{

glm::vec3 Obj::loadCoordinates(std::string line)
{
    glm::vec3 coords;
    static std::regex reg("\\w*+[^ ]++"); // split on spaces
    auto iter_groups = std::sregex_iterator(line.begin(), line.end(), reg);
    auto iter_end = std::sregex_iterator();
    int i = 0;
    for (std::sregex_iterator iter = iter_groups; iter != iter_end; ++iter)
    {
        std::string match = (*iter).str();
        coords[i++] = std::stof(match);
    }
    return coords;
}

std::vector<Geometry::FaceVertex> Obj::loadFaces(std::string line)
{
    std::vector<Geometry::FaceVertex> a_face;
    std::vector<double> coords;
    static std::regex space_reg("\\w*+[^ ]++");
    auto iter_groups = std::sregex_iterator(line.begin(), line.end(), space_reg);
    auto iter_end = std::sregex_iterator();

    // we split the face by vertex (ie we split on spaces)
    for (std::sregex_iterator iter = iter_groups; iter != iter_end; ++iter)
    {
        Geometry::FaceVertex face_vertex;
        std::string token = (*iter).str();
        // First part of the token is for sure the vertex id
        face_vertex.vertexID = std::stoi(token);
        // Now we need to check for texture coordinates and normals
        static std::regex uv_reg("\\d+\\/(\\d+)");
        std::smatch tmp_match;
        if (std::regex_search(token, tmp_match, uv_reg))
        {
            face_vertex.uvID = std::stoi(tmp_match[tmp_match.size() - 1]);
        }
        static std::regex normal_reg("([0-9]*)(\\/[0-9]*\\/)([0-9]*)");
        if (std::regex_match(token, tmp_match, normal_reg))
        {
            // Here tmp_match contains at position 0: the full match
            //                            position 1: the vertex id
            //                            position 2: the uv with slashes
            //                            position 3: the normal or empty
            if (!std::string(tmp_match[3]).empty())
                face_vertex.normalID = std::stoi(tmp_match[3]);
        }
        a_face.push_back(face_vertex);
    }
    return a_face;
}

bool Obj::writeMesh(const std::string& filename)
{
    std::ofstream file(filename, std::ios::out);
    if (!file.is_open())
        return false;

    bool write_normals = normals().size() != 0 ? 1 : 0;
    bool write_uvs = uvs().size() != 0 ? 1 : 0;

    for (const auto& vertex : vertices())
    {
        file << "v " << vertex.x << " " << vertex.y << " " << vertex.z << '\n';
    }

    for (const auto& uv : uvs())
    {
        file << "vt " << uv.x << " " << uv.y << '\n';
    }

    for (const auto& normal : normals())
    {
        file << "vn " << normal.x << " " << normal.y << " " << normal.z << '\n';
    }

    for (const auto& f : faces())
    {
        file << "f";
        for (const auto& face_vertex : f)
        {
            file << " ";
            file << face_vertex.vertexID << "/";
            if (write_uvs)
                file << face_vertex.vertexID;
            file << "/";
            if (write_normals)
                file << face_vertex.normalID;
        }
        file << '\n';
    }

    _log->message("Obj::writeMesh:: - File: % successfully written", filename);
    return true;
}

bool Obj::readMesh(const std::string& filename)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec2> uvs;
    std::vector<glm::vec3> normals;
    std::vector<std::vector<FaceVertex>> faces;

    // Load obj file
    std::ifstream obj_file(filename, std::ios::in);
    if (!obj_file.is_open())
    {
        _log->error("Obj::readMesh - %  could not be opened", filename);
        return false;
    }

    for (std::string line; std::getline(obj_file, line);)
    {
        std::string::size_type pos;
        if ((pos = line.find("v ")) == 0)
        {
            pos += 2;
            line = line.substr(pos);
            vertices.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("vt ")) == 0)
        {
            pos += 3;
            line = line.substr(pos);
            uvs.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("vn ")) == 0)
        {
            pos += 3;
            line = line.substr(pos);
            normals.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("f ")) == 0)
        {
            pos += 2;
            line = line.substr(pos);
            faces.push_back(loadFaces(line));
        }
    }

    _vertices = vertices;
    _normals = normals;
    _uvs = uvs;
    _faces = faces;
    _log->message("Obj::readMesh:: - File: % successfully read", filename);

    return true;
}
}; // namespace calimiro
