/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <cassert>
#include <cmath>
#include <iostream>
#include <memory>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/vector_angle.hpp>

#include "../calimiro/camera.h"
#include "../calimiro/camera_model.h"
#include "../calimiro/estimator.h"
#include "../calimiro/logger.h"

using namespace calimiro;

/**
 * Add radial distortion according to Brown's model with only one coefficient, k1.
 * The reference system is [-1,1] -> the lower left corner is [-1, -1]
 */
glm::dvec2 addDistortion(glm::dvec2 undist, double k1 = 0.02)
{
    double r2 = pow(undist.x, 2.0) + pow(undist.y, 2.0);
    double L_r = 1.0f + k1 * r2;
    glm::dvec2 distorted = undist * L_r;
    return distorted;
}

/**
 * This test is based on the scene of the Blender file check_radial_k1.blend in calimiro/tests/data/check_radial_k1.blend
 * The camera is at the origin, the up is the positive z-axis. The target is the positive y-axis.
 */
int main()
{
    std::vector<calimiro::CalibrationPoint> pts;

    // Points and their 2D correspondence for 3 intersecting planes
    // Note that the upper left corner is pixel (0, 1080)
    // Extreme left
    pts.push_back({glm::dvec3{-1.2627, 3.7373, 0.71429}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{60.0, 1048.0})))});
    pts.push_back({glm::dvec3{-1.0102, 3.9898, 0}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{285.0, 540.0})))});
    pts.push_back({glm::dvec3{-1.2627, 3.7373, -0.71429}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{60.0, 34.0})))});

    // Around 1/4
    pts.push_back({glm::dvec3{-0.75761, 4.2424, 0.71429}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{483.0, 988.0})))});
    pts.push_back({glm::dvec3{-0.50508, 4.4949, 0.35714}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{659.0, 759.0})))});
    pts.push_back({glm::dvec3{-0.75761, 4.2424, -0.71429}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{483.0, 94.0})))});

    // Middle
    pts.push_back({glm::dvec3{0.0, 5.0, 1.0}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{957.0, 1073.0})))});
    pts.push_back({glm::dvec3{0.0, 5.0, 0.0}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{957.0, 542.0})))});
    pts.push_back({glm::dvec3{-0.35714, 4.2424, -0.75761}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{733.0, 69.0})))});

    // Around 3/4
    pts.push_back({glm::dvec3{0.66666667, 5.0, 0.66666667}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1310.0, 897.0})))});
    pts.push_back({glm::dvec3{0.71429, 5.0, 0.0}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1335.0, 542.0})))});
    pts.push_back({glm::dvec3{0.35714, 4.2424, -0.75761}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1179.0, 68.0})))});

    // Extreme right
    pts.push_back({glm::dvec3{1.66666667, 5.0, 1.0}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1841.0, 1073.0})))});
    pts.push_back({glm::dvec3{1.4286, 4.7475, -0.25254}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1755.0, 401.0})))});
    pts.push_back({glm::dvec3{1.4286, 4.2424, -0.75761}, Camera::toScreen(addDistortion(Camera::toNDC(glm::dvec2{1850.0, 69.0})))});

    // Initilisation of the kernel
    double width = 1920.0;
    double height = 1080.0;
    cameramodel::Pinhole_Radial_k1 camera(width, height);
    Logger logger;
    Kernel kern(&logger, std::make_shared<cameramodel::Pinhole_Radial_k1>(camera), pts);
    std::vector<int> vec_inliers;
    std::vector<std::vector<double>> params;
    int seed = 2351996045;  // working random seed for RANSAC
    std::vector<double> param = kern.Ransac(vec_inliers, 0.6, seed);

    if (param.size() == 0)
    {
        std::cout << "\ncalimiro::tests::check_radial_k1 - No camera parameters found" << std::endl;
        return 1;
    }

    // Camera has a vertical fov of 22.90 degrees with cx = cy = 0.5 and k1 = 0.02
    // Verify that the estimation of the fov, cx, cy and k1 are reasonnable
    float fov_estimated = param[0];
    float cx_estimated = param[1];
    float cy_estimated = param[2];
    float k1_estimated = param[3];

    std::cout << "Estimated fov: " << fov_estimated << "\n"
              << "cx: " << cx_estimated << "\n"
              << "cy: " << cy_estimated << "\n"
              << "k1: " << k1_estimated << std::endl;

    assert((fov_estimated > 19.0f && fov_estimated < 25.0f) == true);
    assert((cx_estimated > 0.4f && cx_estimated < 0.6f) == true);
    assert((cy_estimated > 0.4f && cy_estimated < 0.6f) == true);
    assert((k1_estimated > 0.016f && k1_estimated < 0.024f) == true);

    // Verify the extrinsic parameters (eye, target and up)
    int nb_param = param.size();
    glm::dvec3 estimated_eye{param[nb_param - 6], param[nb_param - 5], param[nb_param - 4]};
    glm::dvec3 estimated_euler{param[nb_param - 3], param[nb_param - 2], param[nb_param - 1]};
    glm::dvec3 true_eye{0.0, 0.0, 0.0};
    glm::dvec3 true_target{0.0, 1.0, 0.0};
    glm::dvec3 true_up{0.0, 0.0, 1.0};
    Camera::Extrinsics estimated_extrinsics = camera.computeExtrinsics(estimated_eye, estimated_euler);

    // Verify eye: check the distance between the estimation and the true value
    std::cout << "Eye: " << estimated_extrinsics.eye[0] << " " << estimated_extrinsics.eye[1] << " " << estimated_extrinsics.eye[2] << "\n";
    assert((glm::distance(estimated_extrinsics.eye, true_eye) < 0.25) == true);

    // Verify target
    // Target is a position -> there are multiple solutions along a line
    // To verify the target estimation, we make it a direction from the origin
    // and measure the angle between the estimated vector and the true value
    glm::dvec3 estimated_target_direction = glm::normalize(glm::dvec3(estimated_extrinsics.target - estimated_extrinsics.eye));
    std::cout << "Target direction: " << estimated_target_direction[0] << " " << estimated_target_direction[1] << " " << estimated_target_direction[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_target_direction, true_target, glm::cross(estimated_target_direction, true_target))) < 0.05) == true);

    // Verify up
    std::cout << "Up: " << estimated_extrinsics.up[0] << " " << estimated_extrinsics.up[1] << " " << estimated_extrinsics.up[2] << "\n";
    assert((abs(glm::orientedAngle(estimated_extrinsics.up, true_up, glm::cross(estimated_extrinsics.up, true_up))) < 0.05) == true);

    return 0;
}
