/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <cmath>
#include <limits>

#include <glm/glm.hpp>

#include "./calimiro/calimiro.h"

int main()
{
    double width = 1920.0;
    double height = 1080.0;
    double fov = 45.0;
    double cx = 0.5;
    double cy = 0.5;
    double near = 1.0;
    double far = 1000.0;

    // Verify the Pinhole class
    auto pinhole = calimiro::cameramodel::Pinhole(width, height, fov, cx, cy, near, far);

    double epsilon = std::numeric_limits<double>::epsilon();
    assert((fabs(width - pinhole.intrinsics().width) < epsilon) == true);
    assert((fabs(height - pinhole.intrinsics().height) < epsilon) == true);
    assert((fabs(fov - pinhole.intrinsics().fov) < epsilon) == true);
    assert((fabs(cx - pinhole.intrinsics().cx) < epsilon) == true);
    assert((fabs(cy - pinhole.intrinsics().cy) < epsilon) == true);
    assert((fabs(near - pinhole.intrinsics().near) < epsilon) == true);
    assert((fabs(far - pinhole.intrinsics().far) < epsilon) == true);

    // Verify the Pinhole_Radial_k1 class
    double k1 = 0.02;
    auto radial = calimiro::cameramodel::Pinhole_Radial_k1(width, height, fov, cx, cy, k1, near, far);
    assert((fabs(k1 - radial.getParams()[3]) < epsilon) == true);

    // Verify the conversion from image/screen plane to Normalized Device Coordinates
    // And from the NDC back to screen coordinates

    // With cx=cy=0.5, bottom left corner should map to -1 in the direction where the resolution is the greatest
    // In our case, it is the width -> bottom left corner in x should be -1
    glm::dvec2 true_image_left{0.0, 0.0};
    glm::dvec2 true_cam_left{-1.0, 0.5625};

    glm::dvec2 cam_left = calimiro::Camera::toNDC(true_image_left, width, height, cx, cy);
    glm::dvec2 image_left = calimiro::Camera::toScreen(true_cam_left, width, height, cx, cy);

    assert((fabs(cam_left.x - true_cam_left.x) < epsilon) == true);
    assert((fabs(image_left.x - true_image_left.x) < epsilon) == true);

    // Bottom right, in the x direction, should map to 1
    glm::dvec2 true_image_right{width, 0.0};
    glm::dvec2 true_cam_right{1.0, 0.5625};

    glm::dvec2 cam_right = calimiro::Camera::toNDC(true_image_right, width, height, cx, cy);
    glm::dvec2 image_right = calimiro::Camera::toScreen(true_cam_right, width, height, cx, cy);

    assert((fabs(cam_left.x - true_cam_left.x) < epsilon) == true);
    assert((fabs(image_right.x - true_image_right.x) < epsilon) == true);

    // Center (width/2, height/2) maps to (0,0)
    glm::dvec2 true_image_center{width / 2.0, height / 2.0};
    glm::dvec2 true_cam_center{0.0, 0.0};

    glm::dvec2 cam_center = calimiro::Camera::toNDC(true_image_center, width, height, cx, cy);
    glm::dvec2 image_center = calimiro::Camera::toScreen(true_cam_center, width, height, cx, cy);

    assert((fabs(cam_left.x - true_cam_left.x) < epsilon) == true);
    assert((fabs(cam_center.x - true_cam_center.x) < epsilon) == true);
    assert((fabs(cam_center.y - true_cam_center.y) < epsilon) == true);
    assert((fabs(image_center.x - true_image_center.x) < epsilon) == true);
    assert((fabs(image_center.y - true_image_center.y) < epsilon) == true);
}
